// Stores human friendly names for the GIDs(Global Tile IDs) that was assigned by Tiled
var TileType = Object.freeze({ 
	emptyTile: 0,
	friendlyTile: 1, 
	friendlyTileTouched: 2, 
	missedTile: 3,
});

var TapWhiteLayer = cc.LayerColor.extend({
	level: null, // Stores the current level
	scrollSpeed: 50, // Stores the scrolling speed
	scrollAcceleration: 10, // Stores the amount of scrolling acceleration
	lastWhiteTileTouched: null, // Stores the last tile that was touched, used to validate next player move
	scoreLabel: null, // Stores the score label
	score: 0, // Stores current score
	isDead: false, // A flag determining if the player is dead or not
	tileScale: 1, // Default tile scale, this is initialized in the init function
	
	ctor:function () {
		this._super(new cc.Color(0,0,0,0));
		this.init();		
	},

	init:function () {
		this.scheduleUpdate();
		cc.eventManager.addListener({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: true,
			onTouchBegan: this.onTouchBegin,
			onTouchMoved: function () {},
			onTouchEnded: function () {}
		}, this);
			
		var size = cc.winSize;
		
		// Load level, which is in TMX format created in Tiled
		// Note: In the TMX format, the coordinate system starts in top-left corner!
		this.level = new cc.TMXTiledMap(res.whiteTile_level0);
		var layer = this.level.children[0];
		
		// Scale the level to fit the screen
		this.tileScale = size.width / (layer.tileWidth*this.level.mapWidth);
		this.level.setScale(this.tileScale);
		this.addChild(this.level);
		
		this.lastWhiteTileTouched = { x: 0, y: this.level.getMapSize().height - 1 };
		
		this.scoreLabel = new cc.LabelTTF("Score: 0", fonts.normal, 50);
		this.scoreLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
		this.scoreLabel.setFontFillColor(new cc.Color(125, 125, 125, 255));
		this.scoreLabel.attr({
			x: size.width / 2,
			y: size.height - size.height / 8,
		});
		
		this.addChild(this.scoreLabel);	
	},	

	update:function (dt) {
		if(this.isDead)
			return;
		
		// Update level position
		// We move the level to make the scrolling effect
		// This could also have been done by moving a camera.
		this.level.y -= this.scrollSpeed * this.tileScale * dt;
		this.scrollSpeed += 10*dt;
		
		// Check if the last tile that was hit went out of screen
		var tileSize = this.getRealTileSize();
		var tilePosY = Math.floor(this.level.getMapSize().height + (this.level.y / tileSize.height));
		if(this.lastWhiteTileTouched.y > tilePosY)
			this.onDead();
	},
	
	onTouchBegin:function (touch, event) {						
		var target = event.getCurrentTarget();
		
		if(!target.isDead)
			target.handleTouch(target.convertTouchToNodeSpace(touch));

		return true;
	},
	
	handleTouch:function (pos) {
		var level = this.level;
		var layer = level.children[0];

		var mapSize = level.getMapSize();		
		var tileSize = this.getRealTileSize();

		var tilePos = { x: pos.x / tileSize.width, y: (pos.y - level.y) / tileSize.height };
		tilePos.x = Math.floor(tilePos.x);
		tilePos.y = Math.floor(mapSize.height - tilePos.y);

		if(this.isPosInsideRange(tilePos, mapSize)) {
			this.handleTilePressed(layer, tilePos);
		}
		else {
			this.onDead();
		}
	},
	
	handleTilePressed:function (layer, tilePos) {
		var tile = layer.getTileGIDAt(tilePos);
		
		// Check if this was not a miss 
		// and that the next tile touched was right after the previous hit tiled
		if(tile == TileType.friendlyTile && tilePos.y + 1 >= this.lastWhiteTileTouched.y) {
			layer.setTileGID(TileType.friendlyTileTouched, tilePos); 
			this.lastWhiteTileTouched = tilePos;
			this.incrementScore();
		}
		else {
			layer.setTileGID(TileType.missedTile, tilePos);
			this.onDead();
		}
	},
	
	isPosInsideRange:function (pos, size) {
		return pos.x >= 0 && pos.x < size.width &&
				pos.y >= 0 && pos.y < size.height;
	},
	
	incrementScore:function () {
		++this.score;
		this.scoreLabel.setString("Score: " + this.score);
	},
	
	onDead:function () {
		var self = this;		
		var size = cc.winSize;		
		this.isDead = true;
		
		var restartLabel = new cc.LabelTTF("Restart", fonts.normal, 70);
		restartLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
		restartLabel.setFontFillColor(new cc.Color(125, 125, 125, 255));		
		
		var restartItem =  new cc.MenuItemLabel(restartLabel, function () { self.restart(); } );		
		var menu = new cc.Menu(restartItem);
		menu.attr({
			x: size.width / 2,
			y: size.height / 2,         
		});
		
		this.addChild(menu, 1);
	},
	
	restart:function () {
		var parent = this.getParent();
		this.removeFromParent(true);
		parent.addChild(new TapWhiteLayer());
	},
	
	getRealTileSize:function () {
		var tileSize = this.level.getTileSize();
		return cc.size(tileSize.width * this.tileScale, tileSize.height * this.tileScale);
	}
});

var TapWhiteScene = cc.Scene.extend({
	onEnter:function () {
		this._super();
		
		cc.eventManager.addListener(
				{
					event: cc.EventListener.KEYBOARD,
					onKeyPressed:  function(keyCode, event){						
					},
					onKeyReleased: function(keyCode, event){						
						if(keyCode == BackKeyAndroid)
							GoBackToMenu();
					}
				}, this);
		
		var layer = new TapWhiteLayer();
		this.addChild(layer);
	}
});