var fonts =
{
	normal: "Helvetica"
};

var res = {
	mainBG_png: "res/mainBG.png",
	wall_png: "res/wall.png",
	soccer_ball_png: "res/soccer_ball.png",
	whiteTile_level0: "res/WhiteTile/level0.tmx",
	roll_tile_png: "res/Roll/tile.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}