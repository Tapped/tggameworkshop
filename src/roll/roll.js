var RollLayer = cc.LayerColor.extend({
	ballSprite: null,
	ballBody: null,
	accelerationFactor: 10,
	ballRadius: 0,
	wallTiles: null,
	numTilesBetweenRow: 15,
	space: null,
	camPosY: 0,
	camAccelY: 2,
	camVelY: 20,
	walls: null,
	isDead: false,
	lastRow: 0,
	scoreLabel: null, // Stores the score label
	score: 0, // Stores current score
	lastYSinceLastScoreInc: 0,

	ctor:function () {
		this._super(new cc.Color(255,255,255,255));
		this.init();
	},
	
	init:function () {
		this.scheduleUpdate();		
        cc.eventManager.addListener({
            event: cc.EventListener.ACCELERATION,
            callback: this.onAcceleration
        }, this);

    	cc.eventManager.addListener({
		    event: cc.EventListener.KEYBOARD,
		    onKeyPressed:  function(keyCode, event){
		    	var target = event.getCurrentTarget();
		    	if(keyCode == 65)
		    		target.ballBody.applyImpulse(cp.v(-target.accelerationFactor*1, 0), cp.v(0,0));
		    	if(keyCode == 68)
		    		target.ballBody.applyImpulse(cp.v(target.accelerationFactor*1, 0), cp.v(0,0));
		    },
		    onKeyReleased: function(keyCode, event){
		    }
    	}, this);

        var size = cc.winSize;

        this.wallTiles = new cc.SpriteBatchNode(res.roll_tile_png);
        var tileSize = this.getTileSize();

        this.ballSprite = new cc.Sprite(res.soccer_ball_png);        
        var ballScale = (tileSize.width / this.ballSprite.width)*0.6;
        this.ballRadius = ballScale * this.ballSprite.width * 0.5;

        this.ballSprite.attr(
        {
			scale: ballScale
        });        
                
        this.addChild(this.wallTiles);        
        this.addChild(this.ballSprite);        

        this.initPhysics();
        this.populateLevel();

		this.scoreLabel = new cc.LabelTTF("Score: 0", fonts.normal, 50);
		this.scoreLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
		this.scoreLabel.setFontFillColor(new cc.Color(125, 125, 125, 255));
		this.scoreLabel.attr({
			x: size.width / 2,
			y: size.height - size.height / 8,
		});
		this.addChild(this.scoreLabel);
	},

	initPhysics:function() {	
		var size = cc.winSize;	
		this.space = new cp.Space();
		this.space.gravity = cp.v(0, -350);

		this.addScreenWalls();

        this.ballBody = new cp.Body(1, cp.momentForCircle(0.1, 0, this.ballRadius, cp.vzero));
        this.ballBody.p = cp.v(size.width / 2, size.height / 2);
        this.space.addBody(this.ballBody);
        
        var shape = new cp.CircleShape(this.ballBody, this.ballRadius, cp.vzero);
        shape.setElasticity(0.4);
        shape.setFriction(0.2);        
        this.space.addShape(shape);
	},

	addScreenWalls: function() {
		var size = cc.winSize;
        var wallLeftShape = new cp.BoxShape2(this.space.staticBody, 
    	{
    		l: -1, 
    		r: 0, 
    		b: -4294967295, 
    		t: 4294967295
    	});

        wallLeftShape.setElasticity(0.7);
        wallLeftShape.setFriction(0.1);
        this.space.addStaticShape(wallLeftShape);

        var wallRightShape = new cp.BoxShape2(this.space.staticBody, 
    	{
    		l: size.width, 
    		r: size.width + 1, 
    		b: -4294967295, 
    		t: 4294967295
    	});

        wallRightShape.setElasticity(0.7);
        wallRightShape.setFriction(0.1);
        this.space.addStaticShape(wallRightShape);
	},

	populateLevel:function () {
		var size = cc.winSize;
		var tileSize = this.getTileSize();		

		var numColumnsPerScreenX = Math.ceil(size.width / tileSize.width);
		var numRowsPerScreenY = Math.ceil((size.height / tileSize.height) / this.numTilesBetweenRow) + 1;
		this.walls = new Array(numColumnsPerScreenX);		
		for(var i = 0;i < this.walls.length;++i) {
			this.walls[i] = new Array(numRowsPerScreenY);
		}

		for(var y = 0;y < numRowsPerScreenY;++y) {
			this.addRow(y, numColumnsPerScreenX, 0);
		}
	},

	onAcceleration:function (acc, event) {
		var target = event.getCurrentTarget();
		target.ballBody.applyImpulse(cp.v(-acc.y * target.accelerationFactor, 0), cp.v(0,0));
	},

	update:function (dt) {
		if(this.isDead)
			return;

		this.addNewRowIfNeeded();

		this.space.step(dt);
		this.camVelY += this.camAccelY * dt;
		this.camPosY += this.camVelY * dt;		
		this.wallTiles.y = this.camPosY;		

		this.ballSprite.x = this.ballBody.p.x;
		this.ballSprite.y = this.ballBody.p.y + this.camPosY;

        if (this.ballSprite.getRotation() != -cc.radiansToDegrees(this.ballBody.a)) {
            this.ballSprite.setRotation(-cc.radiansToDegrees(this.ballBody.a));
        }

		var size = cc.winSize;
		if(this.ballSprite.y + this.ballRadius >= size.height) {
			// We reached top
			this.kill();
		}

		if(this.ballSprite.y - this.ballRadius <= 0) {
			// We reached bottom
			this.ballBody.p.y = -this.camPosY + this.ballRadius;
			this.ballBody.vy = 0;
		}

		if(Math.abs(this.ballBody.p.y - this.lastYSinceLastScoreInc) >= 20) {
			this.incrementScore();
			this.lastYSinceLastScoreInc = this.ballBody.p.y;
		}		
	},

	incrementScore:function () {
		this.score += 100;
		this.scoreLabel.setString("Score: " + this.score);
	},

	kill: function () {
		var self = this;		
		var size = cc.winSize;		
		this.isDead = true;
		
		var restartLabel = new cc.LabelTTF("Restart", fonts.normal, 70);
		restartLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
		restartLabel.setFontFillColor(new cc.Color(125, 125, 125, 255));		
		
		var restartItem =  new cc.MenuItemLabel(restartLabel, function () { self.restart(); } );		
		var menu = new cc.Menu(restartItem);
		menu.attr({
			x: size.width / 2,
			y: size.height / 2,         
		});
		
		this.addChild(menu, 1);
	},

	restart:function () {
		var parent = this.getParent();
		this.removeFromParent(true);
		parent.addChild(new RollLayer());
	},

	addNewRowIfNeeded:function () {
		var tileSize = this.getTileSize();

		var curRow = Math.ceil(this.camPosY / (this.numTilesBetweenRow * tileSize.height));
		if(this.lastRow == curRow)
			return;

		this.lastRow = curRow;

		var lastRowY = this.walls[0][0];
		if(lastRowY == null)
			lastRowY = this.walls[0][1];

		for(var x = 0;x < this.walls.length;++x) {
			var upperRow = this.walls[x][this.walls[x].length - 1];
			if(upperRow == null)
				continue;

			this.wallTiles.removeChild(upperRow);				
		}

		// Shift all rows upwards
		for(var x = 0;x < this.walls.length;++x)
		{
			for(var y = this.walls[x].length - 1;y > 0;--y)
			{
				this.walls[x][y] = this.walls[x][y-1];
			}

			this.walls[x][0] = null;
		}
		
		this.addRow(0, this.walls.length, -curRow * this.numTilesBetweenRow * tileSize.height);
	},

	addRow:function (row, numColumnsPerScreenX, yOffset) {
		var tileSize = this.getTileSize();
		var tileToRemove = Math.floor(Math.random() * numColumnsPerScreenX);
		for(var x = 0;x < numColumnsPerScreenX;++x) {		
			if(x == tileToRemove)
				continue;

			var wall = this.walls[x][row] = new cc.Sprite(res.roll_tile_png);
			wall.attr({
				anchorX: 0,
				anchorY: 0,
				x: x * tileSize.width,
				y: row * tileSize.height * this.numTilesBetweenRow + yOffset,
			});
			
			this.wallTiles.addChild(wall);
			this.addWallToPhysicSpace(wall);
		}
	},

	addWallToPhysicSpace:function(wall) {
		var tileSize = this.getTileSize();
        var wallShape = new cp.BoxShape2(this.space.staticBody, 
    	{
    		l: wall.x, 
    		r: wall.x + tileSize.width, 
    		b: wall.y, 
    		t: wall.y + tileSize.height
    	});

        wallShape.setElasticity(0.7);
        wallShape.setFriction(0.1);
        this.space.addStaticShape(wallShape);
	},

	getTileSize:function () {
		return cc.size(this.wallTiles.scaleX * this.wallTiles.getTexture().width, this.wallTiles.scaleY * this.wallTiles.getTexture().height);
	}
});

var RollScene = cc.Scene.extend({
	onEnter:function () {
		this._super();		
		cc.eventManager.addListener(
				{
					event: cc.EventListener.KEYBOARD,
					onKeyPressed:  function(keyCode, event){						
					},
					onKeyReleased: function(keyCode, event){						
						if(keyCode == BackKeyAndroid) {
							cc.inputManager.setAccelerometerEnabled(false);
							GoBackToMenu();							
						}
					}
				}, this);		

		cc.inputManager.setAccelerometerEnabled(true);
		var layer = new RollLayer();
		this.addChild(layer);
	}
});