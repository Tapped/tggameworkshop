var WallFace = Object.freeze({ 
	left: {}, 
	right: {}, 
	top: {}, 
	bottom: {}
	});

var HitBallLayer = cc.LayerColor.extend({
		
	wallSize: cc.size(32, 32), // Wall tile size
	ball: null, // Stores the ball sprite handle
	ballSpeed: 200, // Speed of the ball in pixels per second
	ballDir: cc.p(0.5, 0.5), // Stores current ball direction. This is a 2D vector. Initially it will move obliquely right-up.
	scoreLabel: null, // Stores the score label
	score: 0, // Stores current score
	
	ctor:function () {
		this._super(new cc.Color(255,255,255,255));
		this.init();
	},
	
	init:function () {				
		this.scheduleUpdate();
		cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegin,
            onTouchMoved: function () {},
            onTouchEnded: function () {}
        }, this);
		
		var size = cc.winSize;
		
		this.scoreLabel = new cc.LabelTTF("Score: 0", fonts.normal, 20);
		this.scoreLabel.setHorizontalAlignment(cc.TEXT_ALIGNMENT_CENTER);
		this.scoreLabel.setFontFillColor(new cc.Color(0, 0, 0, 255));
		this.scoreLabel.attr({
			x: size.width / 2,
			y: size.height - size.height / 8,
		});
		this.addChild(this.scoreLabel);		
		
		this.ball = new cc.Sprite(res.soccer_ball_png);
		this.ball.attr({
			x: size.width / 2,
			y: size.height / 2,
			scale: 0.8
		});
			
		this.addChild(this.createWall());	
		this.addChild(this.ball);		
	},	
	
	update:function (dt) {
		var velocity = cc.pMult(this.ballDir, this.ballSpeed * dt);
		this.ball.x += velocity.x;
		this.ball.y += velocity.y;
		
		var collisionFace = this.ballCollideWithWall(); 
		if(collisionFace != null) {	
			// Backtrack to the point before collision
			this.ball.x -= velocity.x;
			this.ball.y -= velocity.y;
			
			// Update ball direction and reflect it as a result of colliding
			switch(collisionFace) {
				case WallFace.left:
					this.ballDir.x = -this.ballDir.x; 
					break;
				case WallFace.right:
					this.ballDir.x = -this.ballDir.x;
					break;
				case WallFace.top:
					this.ballDir.y = -this.ballDir.y;
					break;
				case WallFace.bottom:
					this.ballDir.y = -this.ballDir.y;
					break;
			}
		}
	},
	
	onTouchBegin: function (touch, event) {
		var target = event.getCurrentTarget();
		
		var pos = target.convertTouchToNodeSpace(touch);		
		var toBall = cc.pSub(target.ball.getPosition(), pos);
		var radius = target.getBallRadius();
		
		// Check if touch point is inside circle.
		// This is done in square space.
		// Circle formula: x^2 + y^2 = r^2
		if(toBall.x*toBall.x + toBall.y*toBall.y <= radius*radius) {
			// Update score
			target.score += 1;
			target.scoreLabel.setString("Score: " + target.score);	
		}
		
		return true;
	},
	
	ballCollideWithWall:function () {
		var winSize = cc.winSize;
		
		var actualWallWidth = this.wallSize.width;
		var actualWallHeight = this.wallSize.height;
		var ballRadius = this.getBallRadius();
		
		if(this.ball.x - ballRadius <= actualWallWidth)
			return WallFace.left;
		if(this.ball.x + ballRadius >= winSize.width - actualWallWidth)
			return WallFace.right;
		if(this.ball.y - ballRadius <= actualWallHeight)
			return WallFace.bottom;
		if(this.ball.y + ballRadius >= winSize.height - actualWallHeight)
			return WallFace.top;
			
		return null;
	},
	
	getBallRadius: function () {
		return (this.ball.width*this.ball.scaleX) / 2;
	},
	
	createWall:function () {
		var size = cc.winSize;
		
		var walls = new cc.SpriteBatchNode.create(res.wall_png, 100);
		var numWallsPerRow = Math.ceil(size.width / this.wallSize.width);
		var numWallsPerCol = Math.ceil(size.height / this.wallSize.height);		
		var stepSize = cc.size(size.width / numWallsPerRow,
							   size.height / numWallsPerCol);
		
		// Bottom Row
		for(var i = 0;i < numWallsPerRow - 1;++i) {
			var sprite = new cc.Sprite.create(res.wall_png);
			sprite.attr({
				anchorX: 0,
				anchorY: 0,
				x: i*stepSize.width, 
				y: 0,
				scale: this.wallSize.width / sprite.width });
			walls.addChild(sprite);
		}
		
		// Top Row
		for(var i = 0;i < numWallsPerRow;++i) {
			var sprite = new cc.Sprite.create(res.wall_png);
			sprite.attr({
				anchorX: 0,
				anchorY: 1,
				x: i*stepSize.width, 
				y: size.height,
				scale: this.wallSize.width / sprite.width });
			walls.addChild(sprite);
		}
		
		// Left Column
		for(var i = 1;i < numWallsPerCol;++i) {
			var sprite = new cc.Sprite.create(res.wall_png);
			sprite.attr({
				anchorX: 0,
				anchorY: 1,
				x: 0, 
				y: i*stepSize.height,
				scale: this.wallSize.height / sprite.height });
			walls.addChild(sprite);
		}
		
		// Right Column
		for(var i = 1;i < numWallsPerCol;++i) {
			var sprite = new cc.Sprite.create(res.wall_png);
			sprite.attr({
				anchorX: 1,
				anchorY: 1,
				x: size.width, 
				y: i*stepSize.height,
				scale: this.wallSize.height / sprite.height });
			walls.addChild(sprite);
		}
		
		return walls;
	}
});

var HitBallScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        
        cc.eventManager.addListener(
        		{
        			event: cc.EventListener.KEYBOARD,
        			onKeyPressed:  function(keyCode, event){						
        			},
        			onKeyReleased: function(keyCode, event){						
        				if(keyCode == BackKeyAndroid)
        					GoBackToMenu();
        			}
        		}, this);
        
        var layer = new HitBallLayer();
        this.addChild(layer);
    }
});