
var MainMenuLayer = cc.LayerColor.extend({
    ctor:function () {
        this._super(new cc.Color(41, 63, 109, 255));

        var size = cc.winSize;

        var bg = new cc.Sprite(res.mainBG_png);
        bg.attr({
            scaleX: size.width / bg.width,
            scaleY: size.width / bg.width,
            anchorX: 0,
            anchorY: -0.1,
            y: 0
        });
        
        this.addChild(bg, 0);

        var startHitBallLabel = new cc.LabelTTF("HitBall", fonts.normal, 40);
        var startHitBallItem = new cc.MenuItemLabel(startHitBallLabel, function () { cc.director.runScene(new HitBallScene()); } );
        startHitBallItem.attr({
            x: size.width / 2,
            anchorX: 0.5,
            anchorY: 0.5
        });

        var startTapWhiteTileLabel = new cc.LabelTTF("Tap White Tile", fonts.normal, 40);
        var startTapWhiteTileItem = new cc.MenuItemLabel(startTapWhiteTileLabel, function () { cc.director.runScene(new TapWhiteScene()); } );
        startTapWhiteTileItem.attr({
        	x: size.width / 2,
        	anchorX: 0.5,
        	anchorY: 0.5
        });
        
        var startRollLabel = new cc.LabelTTF("Roll", fonts.normal, 40);
        var startRollItem = new cc.MenuItemLabel(startRollLabel, function () { cc.director.runScene(new RollScene()); } );
        startRollItem.attr({
        	x: size.width / 2,
        	anchorX: 0.5,
        	anchorY: 0.5
        });
        
        var exitLabel = new cc.LabelTTF("Exit", fonts.normal, 40);        
        var exitItem = new cc.MenuItemLabel(exitLabel);
        exitItem.attr({
            x: size.width / 2,
            anchorX: 0.5,
            anchorY: 0.5
        });

        var menu = new cc.Menu(startHitBallItem, startTapWhiteTileItem, startRollItem, exitItem);
        menu.attr({
            x: size.width / 2,
            y: size.height / 1.5,         
        });

        menu.alignItemsVerticallyWithPadding(20);
        this.addChild(menu, 1);

        var titleLabel = new cc.LabelTTF("TG Game Workshop", fonts.normal, 38);
        titleLabel.attr({
            fillStyle: new cc.Color(235, 200, 115, 1)
        })
        titleLabel.x = size.width / 2;
        titleLabel.y = size.height - (size.height / 6);
        this.addChild(titleLabel, 1);

        return true;
    }
});

var MainMenuScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new MainMenuLayer();
        this.addChild(layer);
    }
});

function GoBackToMenu() {
	cc.director.runScene(new MainMenuScene());
}

